(function(){
 "use strict";
 angular.module('myApp')
  .controller('MainCtrl', function($scope) {

    $scope.items = [
        { value: 'ESM' ,name: 'Arcsight ESM' },
        { value: 'broIntel' ,name: 'Bro Intel' },
        { value: 'CEF' ,name: 'CEF' },
        { value: 'CSV' ,name: 'CSV' },
        { value: 'carbonBlack' ,name: 'Carbon Black' },
        { value: 'Cloudera' ,name: 'Cloudera impala' },
        { value: 'Nitro' ,name: 'Nitro' },
        { value: 'Paloalto' ,name: 'Paloalto Networks' },
        { value: 'Qradar' ,name: 'Qradar' },
        { value: 'qradarApi' ,name: 'Qradar api' },
        { value: 'Splunk' ,name: 'Splunk' },
        { value: 'sysLog' ,name: 'Sys log' }];

    $scope.modVersion = function() {
        if($scope.filterCondition==='ESM'){
            $scope.items2 = [
                { value: '6.5' , name: '6.5' },
                { value: '6.0' , name: '6.0' }];
            $scope.filterCondition2={value: '6.5'}; 
            $scope.esm={ port: 8443 , host: 'localhost' , syslogFacility: 'local0'};
        }else if($scope.filterCondition==='Splunk'){
            $scope.items2 = [
                { value: '6.2' , name: '6.2' },
                { value: '6.0' , name: '6.0' }];
            $scope.filterCondition2={value: '6.2'};
            $scope.splunk={ sshPort: 22 , splunkHome: '/opt/splunk' , host: 'localhost'};
        }
    };

    $scope.onSubmit = function() {
        alert("felicidades");
    };
    
});
  
})();